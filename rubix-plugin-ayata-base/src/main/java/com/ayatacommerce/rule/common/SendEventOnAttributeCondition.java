package com.ayatacommerce.rule.common;

import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.ayatacommerce.rule.util.FulfilmentUtils;
import com.ayatacommerce.rule.util.InvoiceUtils;
import com.ayatacommerce.rule.util.OrderUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import static com.ayatacommerce.rule.util.Constants.Common_Constants.CONDITION_SATISFIED_EVENT;
import static com.ayatacommerce.rule.util.Constants.Common_Constants.CONDITION_UNSATISFIED_EVENT;
import static com.ayatacommerce.rule.util.Constants.EntityType.*;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.*;


@RuleInfo(
        name = "SendEventOnAttributeCondition",
        description = "Verify attribute with name {"+ATTRIBUTE_NAME +"}  and attribute value {"+ATTRIBUTE_VALUE+"} if attribute name and  value is matched  then send event {"+CONDITION_SATISFIED_EVENT+"} otherwise send event {"+CONDITION_UNSATISFIED_EVENT+"}",
        accepts = {@EventInfo(entityType = ENTITY_TYPE_ORDER),
                  @EventInfo(entityType = ENTITY_TYPE_FULFILMENT),
                  @EventInfo(entityType = ENTITY_TYPE_INVOICE)
         }
)


@ParamString(name = ATTRIBUTE_NAME, description = "Name of the attribute to match")
@ParamString(name = ATTRIBUTE_VALUE, description = "Value of the attribute")
@ParamString(name = CONDITION_SATISFIED_EVENT, description = "Generates this event if attribute name and value is matched")
@ParamString(name = CONDITION_UNSATISFIED_EVENT, description = "Generates this event if attribute not found or attribute value not matched")

@Slf4j
public class SendEventOnAttributeCondition  implements Rule {

    public static String CLASS_NAME = SendEventOnAttributeCondition.class.getSimpleName();

    @Override
    public void run(Context context) {
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, ATTRIBUTE_NAME, ATTRIBUTE_VALUE,CONDITION_SATISFIED_EVENT,CONDITION_UNSATISFIED_EVENT);
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        log.debug(logPrefix);
        String entityType = contextEvent.getEntityType();
        String entityId = contextEvent.getEntityId();
        String entityRef = contextEvent.getEntityRef();
        String configuredAttributeName = context.getProp(ATTRIBUTE_NAME);
        String configuredAttributeValue = context.getProp(ATTRIBUTE_VALUE);
        String conditionSatisfiedEvent = context.getProp(CONDITION_SATISFIED_EVENT);
        String conditionUnSatisfiedEvent = context.getProp(CONDITION_UNSATISFIED_EVENT);
        String eventToGenerate = null;

        switch (entityType){
            case ENTITY_TYPE_ORDER:
                Object orderAttribute = OrderUtils.getAttribute(context, entityId, configuredAttributeName);
                if(orderAttribute != null){
                    if(orderAttribute.toString().equalsIgnoreCase(configuredAttributeValue)){
                        eventToGenerate = conditionSatisfiedEvent;
                    }else{
                        eventToGenerate = conditionUnSatisfiedEvent;
                    }
                }
                break;
            case ENTITY_TYPE_FULFILMENT:
                Object fulfilmentAttribute = FulfilmentUtils.getAttribute(context, entityId, configuredAttributeName);
                if(fulfilmentAttribute != null){
                    if(fulfilmentAttribute.toString().equalsIgnoreCase(configuredAttributeValue)){
                        eventToGenerate = conditionSatisfiedEvent;
                    }else{
                        eventToGenerate = conditionUnSatisfiedEvent;
                    }
                }
                break;
            case ENTITY_TYPE_INVOICE:
                Object invoiceAttribute = InvoiceUtils.getAttribute(context, entityRef, configuredAttributeName);
                if(invoiceAttribute != null){
                    if(invoiceAttribute.toString().equalsIgnoreCase(configuredAttributeValue)){
                        eventToGenerate = conditionSatisfiedEvent;
                    }else{
                        eventToGenerate = conditionUnSatisfiedEvent;
                    }
                }
                break;
            default:
                throw new IllegalArgumentException(logPrefix +" Invalid Type configured");

        }
        if(eventToGenerate != null){
            Event event = contextEvent.toBuilder().name(eventToGenerate).build();
            log.debug(logPrefix + "Generating event ",event);
            context.action().sendEvent(event);
        }

    }

}
