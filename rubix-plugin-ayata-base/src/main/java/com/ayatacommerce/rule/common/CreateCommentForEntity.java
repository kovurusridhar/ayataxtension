package com.ayatacommerce.rule.common;


import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.ayatacommerce.rule.util.CommentUtils;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.COMMENT;


@RuleInfo(
        name = "CreateCommentForEntity",
        description = "Create Comment : {" + COMMENT + "} for Entity",
        accepts = {
                @EventInfo(entityType = "ORDER"),
                @EventInfo(entityType = "FULFILMENT")
        }
)
@ParamString(name = COMMENT, description = "Comment to be added")
@Slf4j
public class CreateCommentForEntity implements Rule {
    @Override
    public void run(Context context) {
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, COMMENT);
        String comment = context.getProp(COMMENT);
        CommentUtils.addCommentToEntity(context, comment);
    }
}
