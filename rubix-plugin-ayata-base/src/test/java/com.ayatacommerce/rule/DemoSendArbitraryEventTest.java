package com.ayatacommerce.rule;

import com.ayatacommerce.rule.util.Constants;
import com.fluentretail.api.model.EntityType;
import com.fluentretail.api.model.RubixEntity;
import com.fluentretail.api.v2.client.ReadOnlyFluentApiClient;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.test.TestContext;
import com.fluentretail.rubix.v2.test.TestExecutor;
import com.fluentretail.rubix.workflow.RuleSet;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.mockito.Mock;

import java.util.UUID;

import static com.fluentretail.rubix.test.TestUtils.ruleInstance;
import static com.fluentretail.rubix.test.TestUtils.ruleSet;
import static org.junit.Assert.assertEquals;

public class DemoSendArbitraryEventTest {

    @Mock
    private ReadOnlyFluentApiClient testApiClient;

    @Test
    public void testSendEventTriggersAnotherRuleSet() {
        // Given
        String ref = UUID.randomUUID().toString();
        Event event = Event.builder()
                .entityType(EntityType.FULFILMENT.toString())
                .entityId("1")
                .entityRef(ref)
                .rootEntityRef(ref)
                .rootEntityType(EntityType.FULFILMENT.toString())
                .build();

        RuleSet ruleSet = ruleSet(
                event,
                ruleInstance(DemoSendArbitraryEvent.class.getAnnotation(RuleInfo.class).name(), ImmutableMap.of(
                        Constants.EVENT_NAME_PARAMETER_NAME, "TESTEVENT"
                ))
        );
        RuleSet ruleSet2 = ruleSet("second_event",
                ruleInstance(DemoSendArbitraryEvent.class.getAnnotation(RuleInfo.class).name(), ImmutableMap.of(
                        "eventName", "third_event"
                )));

        RubixEntity entity = RubixEntity.builder()
                .id("1")
                .status("status")
                .entityType(EntityType.FULFILMENT.toString())
                .flexType("flexType")
                .flexVersion(1)
                .ref(ref)
                .build();

        TestExecutor executor = TestExecutor.builder()
                .rule(DemoSendArbitraryEvent.class)
                .entity(entity)
                .ruleset(ruleSet)
                .ruleset(ruleSet2)
                .testApiClient(testApiClient)
                .build();

        executor.validateWorkflow(event);

        // When
        TestContext result = executor.execute(event);

        // Then
        assertEquals(1, result.countActions());
    }
}