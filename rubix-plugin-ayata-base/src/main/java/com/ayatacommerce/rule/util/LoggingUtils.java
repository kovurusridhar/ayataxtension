package com.ayatacommerce.rule.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fluentretail.rubix.event.Event;
import org.apache.commons.lang3.StringUtils;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.ayatacommerce.rule.util.Constants.BLANK;


public class LoggingUtils {

    private LoggingUtils(){}
    /*
     * A little logging facility.  Url could point to mockable.io for example
     */
    public static void logValue(String endpoint, String name, String value) {
        if (value == null) {
            value = "null";
        }
        HttpURLConnection conn = null;
        try {
            value = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
            String fullUrl = endpoint + "?" + name + "=" + value;
            URL url = new URL(fullUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.getResponseCode();
        } catch (Exception ex) {

        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public static void postValue(String endpoint, Object value) {
        postValue(endpoint, value, BLANK);
    }

    /*
     * A little logging facility.  Url could point to mockable.io for example
     * This uses post, and converts objects to strings using JsonUtils.
     */
    public static void postValue(String endpoint, Object value, String description) {
        if (value == null) {
            value = "null";
        }
        HttpURLConnection conn = null;
        try {
            JsonNode postValue = JsonUtils.convertPojoToJson(value);
            URL url = new URL(endpoint);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; utf-8");
            conn.setDoOutput(true);
            OutputStream out = conn.getOutputStream();
            String postValueAsString = postValue.toString();
            if (StringUtils.isNotBlank(description)) {
                postValueAsString = description + postValueAsString;
            }
            byte[] input = postValueAsString.getBytes(StandardCharsets.UTF_8);
            out.write(input);
            conn.getResponseCode();
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public static void postEventValues(String endpoint, Event value, String description) {
        Map<String, Object> mapOfValues = new HashMap<>();
        mapOfValues.put("description", description);
        mapOfValues.put("name", value.getName());
        mapOfValues.put("retailerId", value.getRetailerId());
        mapOfValues.put("accountId", value.getAccountId());
        mapOfValues.put("entityId", value.getEntityId());
        mapOfValues.put("entityRef", value.getEntityRef());
        mapOfValues.put("entityType", value.getEntityType());
        mapOfValues.put("entityStatus", value.getEntityStatus());
        mapOfValues.put("entitySubtype", value.getEntitySubtype());
        mapOfValues.put("rootEntityId", value.getRootEntityId());
        mapOfValues.put("rootEntityRef", value.getRootEntityRef());
        mapOfValues.put("rootEntityType", value.getRootEntityType());
        mapOfValues.put("attributes", value.getAttributes());
        postValue(endpoint, mapOfValues);
    }
}
