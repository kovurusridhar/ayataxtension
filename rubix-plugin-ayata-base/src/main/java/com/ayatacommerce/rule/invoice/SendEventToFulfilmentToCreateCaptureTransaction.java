package com.ayatacommerce.rule.invoice;

import com.ayatacommerce.rule.util.AyataRuleUtils;

import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.invoice.GetInvoiceByRefQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByRefQuery;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

import static com.ayatacommerce.rule.util.Constants.EntityType.ENTITY_TYPE_FULFILMENT;
import static com.ayatacommerce.rule.util.Constants.Fulfilment_Constants.FULFILMENT_ID;
import static com.ayatacommerce.rule.util.Constants.Fulfilment_Constants.FULFILMENT_REF;
import static com.ayatacommerce.rule.util.Constants.Invoice_Constants.AMOUNT_TO_CAPTURE;
import static com.ayatacommerce.rule.util.Constants.Invoice_Constants.INVOICE_REF;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;


@Slf4j
@ParamString(name = EVENT_NAME, description = "Event name")
@RuleInfo(
        name = "SendEventToFulfilmentToCreateCaptureTransaction",
        description = "Sends an event to the Fulfilment from invoice entity with the event name{" + EVENT_NAME + "}",
        accepts = {@EventInfo(entityType = "INVOICE")},
        produces = {@EventInfo(entityType = "FULFILMENT", eventName = "{" + EVENT_NAME + "}")}
)

public class SendEventToFulfilmentToCreateCaptureTransaction implements Rule {
    public static String CLASS_NAME = SendEventToFulfilmentToCreateCaptureTransaction.class.getSimpleName();

    @Override
    public void run(Context context) {
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, EVENT_NAME);

        GetInvoiceByRefQuery invoiceByRefQuery = GetInvoiceByRefQuery.builder().invoiceRef(contextEvent.getEntityRef()).build();
        GetInvoiceByRefQuery.Data invoiceRefData = (GetInvoiceByRefQuery.Data) context.api().query(invoiceByRefQuery);
        if (invoiceRefData != null && invoiceRefData.invoice() != null) {

            GetInvoiceByRefQuery.Invoice invoice = invoiceRefData.invoice();
            Double amountToCapture = invoice.totalAmount().amount();
            GetOrderByRefQuery orderByRefQuery = GetOrderByRefQuery.builder().ref(invoice.order().ref()).build();
            GetOrderByRefQuery.Data orderByRefData = (GetOrderByRefQuery.Data) context.api().query(orderByRefQuery);

            List<GetInvoiceByRefQuery.Attribute> attributeList = invoice.attributes();
            Optional<GetInvoiceByRefQuery.Attribute> fulfilmentId = attributeList.stream().filter(attribute -> attribute.name().equalsIgnoreCase(FULFILMENT_ID)).findAny();
            Optional<GetInvoiceByRefQuery.Attribute> fulfilmentRef = attributeList.stream().filter(attribute -> attribute.name().equalsIgnoreCase(FULFILMENT_REF)).findAny();

            if (orderByRefData.order() != null && fulfilmentId.isPresent() && fulfilmentId.isPresent()) {
                GetOrderByRefQuery.Order order = orderByRefData.order();
                Map<String, Object> attributes = new HashMap<>();
                attributes.put(AMOUNT_TO_CAPTURE, amountToCapture);
                attributes.put(INVOICE_REF, invoiceRefData.invoice().ref());
                Event eventToOrder = Event.builder()
                        .name(context.getProp(EVENT_NAME))
                        .accountId(contextEvent.getAccountId())
                        .retailerId(contextEvent.getRetailerId())
                        .entityId(fulfilmentId.get().value().toString())
                        .rootEntityId(order.id())
                        .rootEntityRef(order.ref())
                        .entityType(ENTITY_TYPE_FULFILMENT)
                        .entityRef(fulfilmentRef.get().value().toString())
                        .entitySubtype(ENTITY_TYPE_FULFILMENT)
                        .scheduledOn(new Date())
                        .attributes(attributes)
                        .build();
                context.action().sendEvent(eventToOrder);
            }
        }
    }
}
