
package com.ayatacommerce.rule.billingAccount;


import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.mutations.CreateBillingAccountMutation;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.billing.GetBillingAccountsByCustomerRefQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.foundation.graphql.type.CreateBillingAccountInput;
import com.fluentretail.rubix.foundation.graphql.type.CustomerKey;
import com.fluentretail.rubix.foundation.graphql.type.RetailerId;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;

import static com.ayatacommerce.rule.util.OrderUtils.getOrderByIdQuery;


@RuleInfo(
        name = "CreateBillingAccountForCustomer",
        description = "Create BillingAccount For Customer if they don’t have one",
        accepts = {@EventInfo(entityType = "ORDER")}

)

public class CreateBillingAccountForCustomer implements Rule {


    @Override
    public void run(Context context) {
        Event event = context.getEvent();
        String retailerId = event.getRetailerId();

        // get the order
        final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                context.getEvent().getEntityId(),
                false,
                true,
                false,
                false,
                false,
                false,
                false,
                false
        );
        GetOrderByIdQuery.Customer customer = orderData.order().customer();
        String customerRef = customer.ref();
        String firstName = customer.firstName();
        String lastName = customer.lastName();
        //get billing accounts with corresponding customerRef
        final GetBillingAccountsByCustomerRefQuery billingAccountsByCustomerRefQuery =
                GetBillingAccountsByCustomerRefQuery.builder().customerRef(customerRef).build();
        final GetBillingAccountsByCustomerRefQuery.Data billingAccountsByCustomerRefData =
                (GetBillingAccountsByCustomerRefQuery.Data) context.api().query(billingAccountsByCustomerRefQuery);

        if (billingAccountsByCustomerRefData.billingAccounts() == null || billingAccountsByCustomerRefData.billingAccounts().edges().size() == 0) {
            RetailerId retailerID = RetailerId.builder().id(retailerId).build();
            CustomerKey customerKey = CustomerKey.builder().ref(customer.ref()).build();
            CreateBillingAccountMutation mutation = CreateBillingAccountMutation.builder().input(CreateBillingAccountInput.builder()
                    .ref(customerRef)
                    .type("DEFAULT")
                    .retailer(retailerID)
                    .name(firstName + " " + lastName)
                    .customer(customerKey)
                    .build())
                    .build();
            context.action().mutation(mutation);
        }

    }
}


