package com.ayatacommerce.rule.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

@Slf4j
public class JsonUtils {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
                .setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE)
                .setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE)
                .setVisibility(PropertyAccessor.CREATOR, JsonAutoDetect.Visibility.NONE);
    }

    private static final Map<Class, ObjectMapper> queryMapperCache = new HashMap<>();

    /**
     * Serialise an Apollo Data class into JSON in a way that strips all the extra metadata
     * that Apollo adds by default.
     *
     * @param value any Apollo data value
     * @return a JsonNode that can be added to a webhook, event, or entity attribute
     */
    public static JsonNode filterMetaFields(final Object value) {
        final Class queryClass = value.getClass();
        if (!queryMapperCache.containsKey(queryClass)) {
            ImmutableList<Class> allQueryClasses = ImmutableList.copyOf(
                    value.getClass().getDeclaringClass().getDeclaredClasses());

            final ObjectMapper mapperInstance = MAPPER.copy();
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            mapperInstance.setDateFormat(dateFormat);
            allQueryClasses.forEach(c -> mapperInstance.addMixIn(c, MyMixin.class));
            mapperInstance.setFilterProvider(REMOVE_APOLLO_META_FIELDS);
            queryMapperCache.put(queryClass, mapperInstance);
        }

        ObjectMapper queryMapper = queryMapperCache.get(queryClass);
        return queryMapper.valueToTree(value);
    }

    public static Map convertJsonToMap(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = null;
        try {
            map = objectMapper.readValue(json, Map.class);
            return map;
        } catch (IOException e) {
            log.error("Unable to convert json to map" + json);
        }
        return map;
    }

    public static JsonNode convertPojoToJson(final Object value) {
        final Class queryClass = value.getClass();
        if (!queryMapperCache.containsKey(queryClass)) {
            ImmutableList<Class> allQueryClasses = ImmutableList.copyOf(
                    value.getClass().getDeclaredClasses());

            final ObjectMapper mapperInstance = MAPPER.copy();
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            mapperInstance.setDateFormat(dateFormat);
            allQueryClasses.forEach(c -> mapperInstance.addMixIn(c, MyMixin.class));
            mapperInstance.setFilterProvider(REMOVE_APOLLO_META_FIELDS);
            queryMapperCache.put(queryClass, mapperInstance);
        }

        ObjectMapper queryMapper = queryMapperCache.get(queryClass);
        return queryMapper.valueToTree(value);
    }

    public static final SimpleFilterProvider REMOVE_APOLLO_META_FIELDS = new SimpleFilterProvider().addFilter(
            "removeMeta", SimpleBeanPropertyFilter.serializeAllExcept("$hashCode",
                    "$hashCodeMemoized", "$toString", "__typename"));

    @JsonFilter("removeMeta")
    private static class MyMixin {
    }
}
