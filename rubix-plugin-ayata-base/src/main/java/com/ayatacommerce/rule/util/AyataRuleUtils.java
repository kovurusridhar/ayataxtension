package com.ayatacommerce.rule.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.event.EventType;
import com.fluentretail.rubix.exceptions.PropertyNotFoundException;
import com.fluentretail.rubix.v2.context.Context;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static com.ayatacommerce.rule.util.Constants.EXCEPTION_MESSAGE;
import static com.ayatacommerce.rule.util.Constants.EXCEPTION_TYPE;


public class AyataRuleUtils {
    private static final String EXCEPTION_MISSING_ENTITY_REF = "Entity reference missing";
    private static final String EXCEPTION_MISSING_ROOT_ENTITY_REF = "Root Entity reference missing";

    private AyataRuleUtils() {
    }

    public static void validateRulePropsIsNotEmpty(Context context, String... ruleProps) {
        if (ruleProps != null) {
            for (String ruleProp : ruleProps) {
                if (null == context.getProp(ruleProp) || context.getProp(ruleProp).isEmpty()) {
                    throw new PropertyNotFoundException(400, ruleProp);
                }
            }
        }
    }
    /**
     * This function intend to validate ruleProps with provided validator logic, will return false
     * if validation failed at any point during the validation.
     *
     * @param validator - a lambda function that defined how to validate with contextSomething
     *        against ruleProps
     * @param contextSomething - a contextSomething object that will be using on validator function
     * @param ruleProps - strings that need to be validate on
     * @return - result that if the validation has been passed
     */
    public static boolean validateRuleProps(BiFunction<Context, String, Boolean> validator,
                                            Context contextSomething, String... ruleProps) {
        if (ruleProps != null) {
            for (String ruleProp : ruleProps) {
                if (!validator.apply(contextSomething, ruleProp)) {
                    throw new PropertyNotFoundException(400, ruleProp);
                }
            }
        }
        return true;
    }

    public static void validateContextRefRootRef(Context context) {
        // validate on the rule props configuration
        if (StringUtils.isEmpty(context.getEntity().getRef())) {
            throw new IllegalArgumentException(EXCEPTION_MISSING_ENTITY_REF);
        }

        if (StringUtils.isEmpty(context.getEvent().getRootEntityRef())) {
            throw new IllegalArgumentException(EXCEPTION_MISSING_ROOT_ENTITY_REF);
        }
    }

    public static Event createExceptionEvent(Context context, Exception e) {
        Map<String, Object> exceptionEventAttributesMap = new HashMap<>();
        exceptionEventAttributesMap.put(EXCEPTION_TYPE, e.getMessage());
        exceptionEventAttributesMap.put(EXCEPTION_MESSAGE, e.getStackTrace());
        return context.getEvent().toBuilder().type(EventType.EXCEPTION).attributes(exceptionEventAttributesMap).build();
    }

    public static ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        return objectMapper;
    }

    public static String buildLogPrefix(String className, final Event event) {
        final String entityId = event.getEntityId();
        final String entityRef = event.getEntityRef();
        final String entityType = event.getEntityType();
        final String accountId = event.getAccountId();
        return "[" + accountId + "][BASE][" + className + "][" + entityType + " Id=" + entityId + ", Ref="
                + entityRef + "]";
    }

}
