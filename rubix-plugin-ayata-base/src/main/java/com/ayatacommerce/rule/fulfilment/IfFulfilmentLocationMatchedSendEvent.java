package com.ayatacommerce.rule.fulfilment;


import com.apollographql.apollo.api.Operation;
import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.fulfilment.GetFulfilmentByIdQuery;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import static com.ayatacommerce.rule.util.Constants.Common_Constants.LOCATION_REF;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;

@RuleInfo(
        name = "IfFulfilmentLocationMatchedSendEvent",
        description = "If Fulfilment Location is matched with the location {"+LOCATION_REF+"} then send event {"+EVENT_NAME+"}",
        accepts = {@EventInfo(entityType = "FULFILMENT")},
        produces = {@EventInfo(entityType = "FULFILMENT", eventName = "{" + EVENT_NAME + "}")}
)

@ParamString(name = LOCATION_REF, description = "Location of Fulfilment being fulfilled")
@ParamString(name = EVENT_NAME, description = "Name of the event")
@Slf4j
public class IfFulfilmentLocationMatchedSendEvent implements Rule {

    public static  String CLASS_NAME = IfFulfilmentLocationMatchedSendEvent.class.getSimpleName();

    @Override
    public void run(Context context){
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, LOCATION_REF,EVENT_NAME);
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);

        String fulfilmentId = contextEvent.getEntityId();
        GetFulfilmentByIdQuery fulfilmentByIdQuery = GetFulfilmentByIdQuery.builder().id(fulfilmentId).build();
        GetFulfilmentByIdQuery.Data fulfilmentData = (GetFulfilmentByIdQuery.Data)context.api().query(fulfilmentByIdQuery);
        if(fulfilmentData != null && fulfilmentData.fulfilment() != null){
            String configuredLocation = context.getProp(LOCATION_REF);
            String eventName = context.getProp(EVENT_NAME);
            String fulfilledLocationRef = fulfilmentData.fulfilment().fromLocation().ref();
            if(fulfilledLocationRef.equalsIgnoreCase(configuredLocation)){
                Event event = contextEvent.toBuilder().name(eventName).build();
                log.debug(logPrefix +" Fulfilment location matched Sending Event ",event);
                context.action().sendEvent(event);
            }
        }
    }
}
