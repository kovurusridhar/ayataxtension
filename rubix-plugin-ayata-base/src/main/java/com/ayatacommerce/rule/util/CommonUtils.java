package com.ayatacommerce.rule.util;

import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.exceptions.RubixException;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.mutations.CreateFulfilmentMutation;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.location.QueryLocationQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.virtualCatalogue.QueryVirtualCataloguesWithRefsQuery;
import com.fluentretail.rubix.foundation.graphql.type.AddressId;
import com.fluentretail.rubix.foundation.graphql.type.CreateFulfilmentInput;
import com.fluentretail.rubix.foundation.graphql.type.CreateFulfilmentItemWithFulfilmentInput;
import com.fluentretail.rubix.foundation.graphql.type.OrderId;
import com.fluentretail.rubix.v2.context.Context;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

import static com.ayatacommerce.rule.util.Constants.Common_Constants.REF;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.PROP_INVENTORY_CATALOGUE_REF;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.VIRTUAL_CATALOGUE;


@Slf4j
public class CommonUtils {

    private CommonUtils() {
    }


    public static Event.Builder prepareEventBuilder(Event incomingEvent, String name) {
        Event.Builder builder = Event.builder()
                .id(UUID.randomUUID())
                .entityId(incomingEvent.getEntityId())
                .entityType(incomingEvent.getEntityType())
                .entityStatus(incomingEvent.getEntityStatus())
                .accountId(incomingEvent.getAccountId())
                .entitySubtype(incomingEvent.getEntitySubtype())
                .retailerId(incomingEvent.getRetailerId())
                .rootEntityId(incomingEvent.getRootEntityId())
                .rootEntityType(incomingEvent.getRootEntityType()).attributes(incomingEvent.getAttributes());

        if (StringUtils.isNotEmpty(name)) {
            builder.name(name);
        }

        return builder;
    }




    private static void filterUnfulfilledOrderItemsByFulfilments(@Nonnull final Map<String, Integer> remainingSkusToFulfil,
                                                                 @Nonnull final GetOrderByIdQuery.FulfilmentEdge edge,
                                                                 @Nullable final List<String> excludedFulfilmentStatuses) {
        final String fulfilmentStatus = edge.fulfilmentNode().status();
        if (null != edge.fulfilmentNode())
        {
            edge.fulfilmentNode().fulfilmentItems().fulfilmentItemEdges().forEach(fulfilmentItemEdge -> {
                final Integer qty = remainingSkusToFulfil.get(fulfilmentItemEdge.fulfilmentItemNode().ref());
                if(qty !=  null){
                    if(CollectionUtils.isNotEmpty(excludedFulfilmentStatuses) && excludedFulfilmentStatuses.contains(fulfilmentStatus)){
                        remainingSkusToFulfil.put(fulfilmentItemEdge.fulfilmentItemNode().ref(), qty - fulfilmentItemEdge.fulfilmentItemNode().requestedQuantity());
                    } else {
                        remainingSkusToFulfil.put(fulfilmentItemEdge.fulfilmentItemNode().ref(), qty - (fulfilmentItemEdge.fulfilmentItemNode().requestedQuantity() - fulfilmentItemEdge.fulfilmentItemNode().rejectedQuantity()));
                    }
                    if(remainingSkusToFulfil.get(fulfilmentItemEdge.fulfilmentItemNode().ref()).equals(0)){
                        remainingSkusToFulfil.remove(fulfilmentItemEdge.fulfilmentItemNode().ref());
                    }
                }
            });
        }
    }


    private static void getOrderItems(final Map<String, Integer> remainingSkusToFulfil, GetOrderByIdQuery.ItemEdge edge) {
        final Integer qty = remainingSkusToFulfil.get(edge.itemNode().ref());
        if (qty != null) {
            remainingSkusToFulfil.put(edge.itemNode().ref(), qty + edge.itemNode().quantity());
        } else {
            remainingSkusToFulfil.put(edge.itemNode().ref(), edge.itemNode().quantity());
        }
    }

    public static boolean isMaxSplitLimitReached(@Nonnull final GetOrderByIdQuery.Fulfilments fulfilments,
                                                 final int maxSplitLimit,
                                                 @Nullable final List<String> excludedFulfilmentStatuses) {
        int validFulfilmentCount=0;
        if(null!=fulfilments && CollectionUtils.isNotEmpty(fulfilments.fulfilmentEdges()))
        {
            if (CollectionUtils.isEmpty(excludedFulfilmentStatuses))
            {
                validFulfilmentCount = (int) fulfilments.fulfilmentEdges().stream().count();
            }
            else
            {
                validFulfilmentCount = (int) fulfilments.fulfilmentEdges().stream().filter(edge1 -> !excludedFulfilmentStatuses.contains(edge1.fulfilmentNode().status())).count();
            }
            log.info("[COMMON-GI] {} Fulfilment count: {} and MaxSplitLimit: {} ", CommonUtils.class.getSimpleName(), validFulfilmentCount, maxSplitLimit);
        }

        return validFulfilmentCount > maxSplitLimit;
    }

    public static String getStoreAddressByLocationRef(Context context, String locationRef) {
        try {
            log.info("[COMMON-GI] {} Getting store address for location ref: {}", CommonUtils.class.getSimpleName(), Lists.newArrayList(locationRef));
            QueryLocationQuery queryLocationForPrimaryAddress = QueryLocationQuery.builder().ref(locationRef).build();
            QueryLocationQuery.Data locationData = (QueryLocationQuery.Data) context.api().query(queryLocationForPrimaryAddress);
            if (null != locationData) {
                return locationData.location().primaryAddress().id();
            }
            log.info("[COMMON-GI] {} store address for location ref: {} is not available.", CommonUtils.class.getSimpleName(), locationData);
            throw new RubixException(404, "Store address id not found for ref: " + locationRef);
        } catch (Exception e) {
            log.error("[COMMON-GI] " + CommonUtils.class.getSimpleName() + " Exception occured", e);
            throw new RubixException(404, "Exception occurred:" + e.getMessage() + " while getting Store address for location ref: " + locationRef);
        }
    }

    public static void createFulfilment(Context context,
                                        String ruleName,
                                        String fromAddressId,
                                        String toAddressId,
                                        String fulfilmentType,
                                        String orderId,
                                        String deliveryType,
                                        List<CreateFulfilmentItemWithFulfilmentInput> items) {
        log.info("[COMMON-GI] rule: {} Create fulfilment for order id: {}, FROM AddressId: {}, TO Address id: {} fulfilment Type: {} delivery Type: {}",
            ruleName, orderId, fromAddressId, toAddressId, fulfilmentType, deliveryType);

        CreateFulfilmentInput createFulfilmentInput = CreateFulfilmentInput.builder().fromAddress(AddressId.builder().id(fromAddressId).build()).toAddress(AddressId.builder().id(toAddressId).build())
            .type(fulfilmentType).items(items).order(OrderId.builder().id(orderId).build()).ref(UUID.randomUUID().toString()).deliveryType(deliveryType).build();
        CreateFulfilmentMutation createFulfilment = CreateFulfilmentMutation.builder().input(createFulfilmentInput).build();
        context.action().mutation(createFulfilment);
    }



    public static void sendEventToVirtualCatalogues(Context context, String eventName, List<String> virtualCatalogueRef, String ipRef,String productRef,String locationRef) {

        final Event event = context.getEvent();
        List<String> catalogueRefList = new ArrayList<String>();
        catalogueRefList.addAll(virtualCatalogueRef);

        log.info("[COMMON-GI] - executing with eventName:{}, catalogueRef:{}, positionRef:{}",
                eventName, context.getEvent().getRootEntityRef(), context.getEvent().getEntityRef());

        // lookup all virtual catalogues
        QueryVirtualCataloguesWithRefsQuery findAllVirtualCatalogues = QueryVirtualCataloguesWithRefsQuery.builder().virtualCatalogueRef(catalogueRefList).build();
        QueryVirtualCataloguesWithRefsQuery.Data data = (QueryVirtualCataloguesWithRefsQuery.Data) context.api().query(findAllVirtualCatalogues);

        // send event to all the virtual catalogues
        List<QueryVirtualCataloguesWithRefsQuery.Edge> edges = data.virtualCatalogues().edges();
        log.info("[COMMON-GI] - event is rootEntityId:{}, rootEntityRef:{}, rootEntityType:{}, entityId:{}, entityRef:{}, entityType:{}, entitySubtype:{}, entityStatus:{}"
                , event.getRootEntityId(), event.getRootEntityRef(), event.getEntityType(), event.getEntityId(), event.getEntityRef(), event.getEntityType(), event.getEntitySubtype(), event.getEntityStatus());

        if (edges != null && edges.size() != 0) {
            for (QueryVirtualCataloguesWithRefsQuery.Edge edge : edges) {
                QueryVirtualCataloguesWithRefsQuery.Node node = edge.node();
                if (node != null) {
                    Event virtualCatalogEvent = context.getEvent().toBuilder()
                            .name(eventName)
                            .scheduledOn(new Date())
                            .entityRef(node.ref())
                            .entityType(VIRTUAL_CATALOGUE)
                            .entitySubtype(node.type())
                            .entityStatus(node.status())
                            .rootEntityRef(node.ref())
                            .rootEntityType(VIRTUAL_CATALOGUE)
                            .attributes(
                                    constructVirtualPositionAttribute(event.getRootEntityRef(), ipRef,productRef,locationRef)
                            )
                            .build();
                    context.action().sendEvent(virtualCatalogEvent);
                }
            }
        } else {
            log.info("No virtual catalogues are available to send event");
        }
    }
    public static Map<String, Object> constructVirtualPositionAttribute(String icRef, String ipRef,String productRef,String locationRef) {

        log.info("[COMMON-GI] - rule: {} - VPAttributes: INVENTORY_CATALOG_REF:{}, INVENTORY_POSITION_REF:{}", icRef, ipRef);
        Map<String, Object> rootAttribute = new HashMap<>();
        if (ipRef != null) {
            rootAttribute.put(PROP_INVENTORY_CATALOGUE_REF, icRef);
            rootAttribute.put(REF, ipRef);
            rootAttribute.put("productRef", productRef);
            rootAttribute.put("locationRef", locationRef);

        }
        return rootAttribute;
    }

}
