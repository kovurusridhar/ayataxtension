package com.ayatacommerce.rule.fulfilment;


import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.mutations.UpdateOrderMutation;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.foundation.graphql.type.AttributeInput;
import com.fluentretail.rubix.foundation.graphql.type.UpdateOrderInput;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ayatacommerce.rule.util.Constants.EntityType.ENTITY_TYPE_STRING;
import static com.ayatacommerce.rule.util.Constants.Order_Constants.FIRST_FULFILMENT_SHIPPED;
import static com.ayatacommerce.rule.util.OrderUtils.getOrderByIdQuery;


@RuleInfo(
        name = "AddAttributeIfFirstFulfilmentToShip",
        description = "Adds an attribute to the order  if this is the first fulfilment to dispatch",
        accepts = {@EventInfo(entityType = "FULFILMENT")}

)

@Slf4j
public class AddAttributeIfFirstFulfilmentToShip implements Rule {

    public static  String CLASS_NAME =AddAttributeIfFirstFulfilmentToShip.class.getSimpleName();

    @Override
    public void run(Context context){
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        String orderId = context.getEvent().getRootEntityId();
        String fulfilmentId = context.getEvent().getEntityId();

        // get the order
        final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                orderId,
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false
        );
        Optional<GetOrderByIdQuery.Attribute> anyConfirmedFulfilment = orderData.order().attributes().stream().filter(orderAttribute -> orderAttribute.name().equalsIgnoreCase(FIRST_FULFILMENT_SHIPPED)).findAny();
        if(!anyConfirmedFulfilment.isPresent()){
            List<AttributeInput> attributeInputList = new ArrayList<>();
            AttributeInput attributeFulfilmentId = AttributeInput.builder().name(FIRST_FULFILMENT_SHIPPED).type(ENTITY_TYPE_STRING).value(fulfilmentId).build();
            attributeInputList.add(attributeFulfilmentId);
            UpdateOrderInput updateOrderInput = UpdateOrderInput.builder().id(orderId).attributes(attributeInputList).build();
            UpdateOrderMutation updateOrderMutation = UpdateOrderMutation.builder().input(updateOrderInput).build();
            log.info(logPrefix +"Adding Attribute");
            context.action().mutation(updateOrderMutation);
        }


    }
}
