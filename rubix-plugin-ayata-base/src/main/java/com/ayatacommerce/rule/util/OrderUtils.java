package com.ayatacommerce.rule.util;

import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByRefQuery;
import com.fluentretail.rubix.v2.context.Context;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

import static com.ayatacommerce.rule.util.Constants.Order_Constants.FinancialTransaction_Constants.AUTHORIZED;


@Slf4j
public class OrderUtils
{
    private OrderUtils(){}

    /**
     *
     * @param context
     * @param orderId
     * @param includeAttributes
     * @param includeCustomer
     * @param includeFulfilmentChoice
     * @param includeFulfilments
     * @param includeOrderItems
     * @param includeArticles
     * @param includeConsignmentArticles
     * @param includeFinancialTransactions
     * @return
     */
    public static GetOrderByIdQuery.Data getOrderByIdQuery(Context context,
                                                           String orderId,
                                                           boolean includeAttributes,
                                                           boolean includeCustomer,
                                                           boolean includeFulfilmentChoice,
                                                           boolean includeFulfilments,
                                                           boolean includeOrderItems,
                                                           boolean includeArticles,
                                                           boolean includeConsignmentArticles,
                                                           boolean includeFinancialTransactions
    )
    {
        int orderItemCount = 25;
        int defaultCount = 25;

        GetOrderByIdQuery.Builder getOrderById = GetOrderByIdQuery.builder()
            .id(orderId)
            .includeAttributes(includeAttributes)
            .includeCustomer(includeCustomer)
            .includeFulfilmentChoice(includeFulfilmentChoice)
            .includeFulfilments(includeFulfilments)
            .includeOrderItems(includeOrderItems)
            .includeArticles(includeArticles)
            .includeConsignmentArticles(includeConsignmentArticles)
            .includeFinancialTransactions(includeFinancialTransactions)
            .articleCount(defaultCount)
            .fulfilmentItemCount(orderItemCount)
            .fulfilmentCount(defaultCount)
            .orderItemCount(orderItemCount)
            .financialTransactionsCount(defaultCount)
            .consignmentArticlesCount(defaultCount);

        return (GetOrderByIdQuery.Data) context.api().query(getOrderById.build());
    }


    /**
     *
     * @param context context of the rule
     * @param orderId id of the order
     * @return returns delivery of the country from fulfilment choice delivery adress
     */
    public static String getDeliveryCountry(Context context,String orderId){
        GetOrderByIdQuery.Data orderData = OrderUtils.getOrderByIdQuery(context, orderId,
                false,
                false,
                true,
                false,
                false,
                false,
                false,
                false);
        return orderData.order().fulfilmentChoice().deliveryAddress().country();
    }

    /**
     *
     * @param context of the rule
     * @returns authorized financial transaction of the order if any
     */

    public static  Optional<GetOrderByIdQuery.Edge> getAnyAuthorizedTransactionByOrderId(Context context){
        final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                context.getEvent().getEntityId(),
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                true
        );
        Optional<GetOrderByIdQuery.Edge> anyAuthorizedTransaction = orderData.order().financialTransactions()
                .edges().stream().filter(financialEdge -> financialEdge.node().ref().contains(AUTHORIZED)).findAny();
        return anyAuthorizedTransaction;
    }

    /**
     *
     * @param context context of the rule
     * @param orderRef  order ref
     * @return returns authroized transaction using order ref
     */
    public static Optional<GetOrderByIdQuery.Edge> getAnyAuthorizedTransactionByOrderRef(Context context,String orderRef){
        GetOrderByIdQuery.Order order = getOrderByRef(context, orderRef);
        Optional<GetOrderByIdQuery.Edge> anyAuthorizedTransaction = order.financialTransactions().edges()
                .stream().filter(financialEdge -> financialEdge.node().ref().contains(AUTHORIZED)).findAny();
        return anyAuthorizedTransaction;

    }

    /**
     *
     * @param context
     * @param orderRef
     * @return returns order
     */
    public static GetOrderByIdQuery.Order getOrderByRef(Context context, String orderRef){
        GetOrderByRefQuery orderByRefQuery = GetOrderByRefQuery.builder().ref(orderRef).build();
        GetOrderByRefQuery.Data orderByRefData = (GetOrderByRefQuery.Data)context.api().query(orderByRefQuery);
        String orderId = orderByRefData.order().id();
        final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                orderId,
                true,
                false,
                true,
                false,
                false,
                false,
                false,
                true
        );
        GetOrderByIdQuery.Order order = orderData.order();
        return  order;

    }

    /**
     *
     * @param context
     * @param orderId
     * @param attributeName
     * @return checks if attribute exists if exist returns true  otherwise false value.
     */

    public static boolean checkIfAttributeExist(Context context,String orderId,String attributeName){
        final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                orderId,
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false
        );
        GetOrderByIdQuery.Order order = orderData.order();
        if(order != null){
            Optional<GetOrderByIdQuery.Attribute> anyAttribute = order.attributes().stream().filter(orderAttribute -> orderAttribute.name().equalsIgnoreCase(attributeName)).findAny();
            return anyAttribute.isPresent();
        }
        return false;
    }

    /**
     *
     * @param context
     * @param orderId
     * @param attributeName
     * @return  if attribute found then returns the attribute value
     */
    public static Object getAttribute(Context context,String orderId,String attributeName){
        final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                orderId,
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false
        );
        GetOrderByIdQuery.Order order = orderData.order();
        if(order != null){
            Optional<GetOrderByIdQuery.Attribute> anyAttribute = order.attributes().stream().filter(orderAttribute -> orderAttribute.name().equalsIgnoreCase(attributeName)).findAny();
            if(anyAttribute.isPresent()){
                return anyAttribute.get().value();
            }
        }
        return null;
    }

    public static String getCustomerRef(Context context , String orderId){
        final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                orderId,
                false,
                true,
                false,
                false,
                false,
                false,
                false,
                false
        );
        if(orderData != null && orderData.order() != null){
            return orderData.order().customer().ref();
        }
        return null;
    }








}