package com.ayatacommerce.rule.util;

import com.apollographql.apollo.api.Operation;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.product.GetStandardProductQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.product.GetVariantProductQuery;
import com.fluentretail.rubix.v2.context.Context;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProductUtils {

    private ProductUtils(){

    }

    /**
     *
     * @param standardProductRef ref of the standard product
     * @param productCatalogueRef ref of the product catalogue
     * @param context context of the rule
     * @return standard product
     */

    public static GetStandardProductQuery.StandardProduct getStandardProduct(String standardProductRef, String productCatalogueRef, Context context){
        GetStandardProductQuery standardProductQuery = GetStandardProductQuery.builder().ref(standardProductRef).catalogue(productCatalogueRef).build();
        GetStandardProductQuery.Data standardProductData = (GetStandardProductQuery.Data)context.api().query(standardProductQuery);
        GetStandardProductQuery.StandardProduct standardProduct = standardProductData.standardProduct();
        return standardProduct;
    }

    /**
     *
     * @param variantProductRef reference of the varaiant product
     * @param catalogueRef reference of the product catalogue
     * @param context context of the rule
     * @return variant product
     */

    public static GetVariantProductQuery.VariantProduct getVariantProduct(String variantProductRef,String catalogueRef,Context context){
        GetVariantProductQuery variantProductQuery = GetVariantProductQuery.builder().ref(variantProductRef).catalogue(catalogueRef).build();
        GetVariantProductQuery.Data variantProductData = (GetVariantProductQuery.Data)context.api().query(variantProductQuery);
        GetVariantProductQuery.VariantProduct variantProduct = variantProductData.variantProduct();
        return variantProduct;
    }


}
