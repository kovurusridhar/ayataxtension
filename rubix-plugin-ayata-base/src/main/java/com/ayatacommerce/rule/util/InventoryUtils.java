package com.ayatacommerce.rule.util;

import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.inventoryPositions.GetInventoryPositionsOfProductsQuery;
import com.fluentretail.rubix.foundation.graphql.type.InventoryCatalogueKey;
import com.fluentretail.rubix.v2.context.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InventoryUtils {
    private InventoryUtils(){

    }

    /**
     *
     * @param productRefs product refs of the product
     * @param inventoryCatalogueRef inventory catalogue ref
     * @param context context of the rule
     * @return return all inventory positions of the product including Out of stock
     */
    public static GetInventoryPositionsOfProductsQuery.InventoryPositions getIPsOfProduct(List<String> productRefs, String inventoryCatalogueRef, Context context){
        GetInventoryPositionsOfProductsQuery inventoryPositionsOfProductsQuery = GetInventoryPositionsOfProductsQuery.builder().productRefs(productRefs).catalogueRef(InventoryCatalogueKey.builder().ref(inventoryCatalogueRef).build()).build();
        GetInventoryPositionsOfProductsQuery.Data inventoryPositionsData = (GetInventoryPositionsOfProductsQuery.Data) context.api().query(inventoryPositionsOfProductsQuery);
        GetInventoryPositionsOfProductsQuery.InventoryPositions inventoryPositions = inventoryPositionsData.inventoryPositions();
        return inventoryPositions;
    }

    /**
     *
     * @param productRef product ref of the product
     * @param inventoryCatalogueRef inventory catalogue ref
     * @param context context of the rule
     * @return returns only available inventory positions
     */
    public static List<GetInventoryPositionsOfProductsQuery.Node> getAvailableIPsOfProduct(String productRef, String inventoryCatalogueRef, Context context){
        List<String> productRefs = new ArrayList<>();
        productRefs.add(productRef);
        GetInventoryPositionsOfProductsQuery inventoryPositionsOfProductsQuery = GetInventoryPositionsOfProductsQuery.builder().productRefs(productRefs).catalogueRef(InventoryCatalogueKey.builder().ref(inventoryCatalogueRef).build()).build();
        GetInventoryPositionsOfProductsQuery.Data inventoryPositionsData = (GetInventoryPositionsOfProductsQuery.Data) context.api().query(inventoryPositionsOfProductsQuery);
        GetInventoryPositionsOfProductsQuery.InventoryPositions inventoryPositions = inventoryPositionsData.inventoryPositions();
        List<GetInventoryPositionsOfProductsQuery.Node> ipNodes = inventoryPositions.edges().stream().filter(inventoryPositionEdge -> inventoryPositionEdge.node().onHand() > 0).map(edge -> edge.node()).collect(Collectors.toList());
        return ipNodes;
    }

    /**
     *
     * @param productRef product ref of the product
     * @param inventoryCatalogueRef inventory catalogue ref
     * @param context context of the rule
     * @return returns only out of stock inventory positions
     */
    public static List<GetInventoryPositionsOfProductsQuery.Node> getOutOfStockIPsOfProduct(String productRef, String inventoryCatalogueRef, Context context){
        List<String> productRefs = new ArrayList<>();
        productRefs.add(productRef);
        GetInventoryPositionsOfProductsQuery inventoryPositionsOfProductsQuery = GetInventoryPositionsOfProductsQuery.builder().productRefs(productRefs).catalogueRef(InventoryCatalogueKey.builder().ref(inventoryCatalogueRef).build()).build();
        GetInventoryPositionsOfProductsQuery.Data inventoryPositionsData = (GetInventoryPositionsOfProductsQuery.Data) context.api().query(inventoryPositionsOfProductsQuery);
        GetInventoryPositionsOfProductsQuery.InventoryPositions inventoryPositions = inventoryPositionsData.inventoryPositions();
        List<GetInventoryPositionsOfProductsQuery.Node> ipNodes = inventoryPositions.edges().stream().filter(inventoryPositionEdge -> inventoryPositionEdge.node().onHand() <= 0).map(edge -> edge.node()).collect(Collectors.toList());
        return ipNodes;
    }

    /**
     *
     * @param productRef of the product
     * @param inventoryCatalogueRef inventory catalogue ref
     * @param status Inventory posistions with the mentioned status
     * @param context context of the rule
     * @return returns inventory positions only with the mentioned status
     */
    public static List<GetInventoryPositionsOfProductsQuery.Node> getIPsOfProductWithStatus(String productRef, String inventoryCatalogueRef,String status, Context context){
        List<String> productRefs = new ArrayList<>();
        productRefs.add(productRef);
        GetInventoryPositionsOfProductsQuery inventoryPositionsOfProductsQuery = GetInventoryPositionsOfProductsQuery.builder().productRefs(productRefs).catalogueRef(InventoryCatalogueKey.builder().ref(inventoryCatalogueRef).build()).build();
        GetInventoryPositionsOfProductsQuery.Data inventoryPositionsData = (GetInventoryPositionsOfProductsQuery.Data) context.api().query(inventoryPositionsOfProductsQuery);
        GetInventoryPositionsOfProductsQuery.InventoryPositions inventoryPositions = inventoryPositionsData.inventoryPositions();
        List<GetInventoryPositionsOfProductsQuery.Node> ipNodes = inventoryPositions.edges().stream().filter(inventoryPositionEdge -> inventoryPositionEdge.node().status().equalsIgnoreCase(status) ).map(edge -> edge.node()).collect(Collectors.toList());
        return ipNodes;
    }
}
