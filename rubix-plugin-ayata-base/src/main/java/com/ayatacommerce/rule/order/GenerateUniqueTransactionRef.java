package com.ayatacommerce.rule.order;


import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.ayatacommerce.rule.util.Constants.COLON;
import static com.ayatacommerce.rule.util.Constants.Order_Constants.FinancialTransaction_Constants.FINANCIAL_TRANSACTION_REF;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.*;

@RuleInfo(name = "GenerateUniqueTransactionRef",  description = "Generates unique transaction Ref" +
        " for transaction type {"+TRANSACTION_TYPE+"} and generates event {"+EVENT_NAME+"}",
        accepts = {@EventInfo(entityType = "ORDER")},
        produces = {
                @EventInfo(eventName = "{" + EVENT_NAME + "}")
        }
)

@ParamString(name = TRANSACTION_TYPE, description = "financial transaction type")
@ParamString(name = EVENT_NAME, description = "name of the event to generate")

@Slf4j
public class GenerateUniqueTransactionRef implements Rule {
    public static  String CLASS_NAME = GenerateUniqueTransactionRef.class.getSimpleName();

    @Override
    public void run(Context context){
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, TRANSACTION_TYPE,EVENT_NAME);
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        String transactionType = context.getProp(TRANSACTION_TYPE);
        String eventName = context.getProp(EVENT_NAME);
        String uniqueUUID = UUID.randomUUID() + COLON + transactionType.toUpperCase();
        Map<String, Object> attributes = getAttributes(contextEvent);
        attributes.put(FINANCIAL_TRANSACTION_REF,uniqueUUID);

        Event event = contextEvent.toBuilder().name(eventName).attributes(attributes).build();
        log.debug(logPrefix +" Sending event : {}",event);
        context.action().sendEvent(event);
    }

    private Map<String,Object> getAttributes(Event event){
        Map<String, Object> attributes = event.getAttributes();
        if(MapUtils.isEmpty(attributes)){
            attributes = new HashMap<>();
        }
        return attributes;
    }
}
