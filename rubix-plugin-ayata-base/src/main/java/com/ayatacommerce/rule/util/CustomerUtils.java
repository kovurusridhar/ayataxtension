package com.ayatacommerce.rule.util;

import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.fulfilment.GetFulfilmentByIdQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.v2.context.Context;

import static com.ayatacommerce.rule.util.OrderUtils.getOrderByIdQuery;

public class CustomerUtils {
    private CustomerUtils(){
    }

    public String getCustomerRef(Context context, GetOrderByIdQuery.Order order){
        return OrderUtils.getCustomerRef(context,order.id());
    }
    public String getCustomerRef(Context context, GetFulfilmentByIdQuery.Fulfilment fulfilment){
        return OrderUtils.getCustomerRef(context,fulfilment.order().id());
    }

}
