package com.ayatacommerce.rule.invoice;


import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.invoice.GetInvoiceByRefQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.ayatacommerce.rule.util.Constants.EntityType.ENTITY_TYPE_ORDER;
import static com.ayatacommerce.rule.util.Constants.Invoice_Constants.AMOUNT_TO_CAPTURE;
import static com.ayatacommerce.rule.util.Constants.Invoice_Constants.INVOICE_REF;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;
import static com.ayatacommerce.rule.util.OrderUtils.getAnyAuthorizedTransactionByOrderRef;
import static com.ayatacommerce.rule.util.OrderUtils.getOrderByRef;

@RuleInfo(
        name = "SendEventToOrderToCreateCaptureTransaction",
        description = "Sends an event to the order from invoice entity with the event name{" + EVENT_NAME + "}",
        accepts = {@EventInfo(entityType = "INVOICE")},
        produces = {@EventInfo(entityType = "ORDER", eventName = "{" + EVENT_NAME + "}")}
)

@Slf4j
@ParamString(name = EVENT_NAME, description = "Event name")

public class SendEventToOrderToCreateCaptureTransaction implements Rule {
    public static String CLASS_NAME = SendEventToOrderToCreateCaptureTransaction.class.getSimpleName();

    @Override
    public void run(Context context) {
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, EVENT_NAME);

        GetInvoiceByRefQuery invoiceByRefQuery = GetInvoiceByRefQuery.builder().invoiceRef(contextEvent.getEntityRef()).build();
        GetInvoiceByRefQuery.Data invoiceRefData = (GetInvoiceByRefQuery.Data) context.api().query(invoiceByRefQuery);

        if (invoiceRefData != null && invoiceRefData.invoice() != null) {
            GetInvoiceByRefQuery.Invoice invoice = invoiceRefData.invoice();
            Double amountToCapture = invoice.totalAmount().amount();
            GetInvoiceByRefQuery.Order order = invoice.order();
            Optional<GetOrderByIdQuery.Edge> anyAuthorizedTransaction = getAnyAuthorizedTransactionByOrderRef(context, invoice.order().ref());
            if (anyAuthorizedTransaction.isPresent()) {
                Map<String, Object> attributes = new HashMap<>();
                attributes.put(AMOUNT_TO_CAPTURE, amountToCapture);
                attributes.put(INVOICE_REF, invoiceRefData.invoice().ref());
                GetOrderByIdQuery.Order orderById = getOrderByRef(context, invoice.order().ref());
                Event eventToOrder = Event.builder()
                        .name(context.getProp(EVENT_NAME))
                        .accountId(contextEvent.getAccountId())
                        .retailerId(contextEvent.getRetailerId())
                        .entityId(orderById.id())
                        .rootEntityId(orderById.id())
                        .rootEntityRef(order.ref())
                        .entityType(ENTITY_TYPE_ORDER)
                        .entityRef(order.ref())
                        .entityStatus(orderById.status())
                        .entitySubtype(orderById.type())
                        .scheduledOn(new Date())
                        .attributes(attributes)
                        .build();
                context.action().sendEvent(eventToOrder);
            }
        }
    }
}
