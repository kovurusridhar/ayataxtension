package com.ayatacommerce.rule.util;

import com.apollographql.apollo.api.Operation;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.fulfilment.GetFulfilmentByIdQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.fulfilment.GetFulfilmentByRefQuery;
import com.fluentretail.rubix.v2.context.Context;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class FulfilmentUtils {

    private FulfilmentUtils(){

    }

    /**
     *
     * @param context
     * @param fulfilmentId
     * @return fulfilemnt
     */
    public static GetFulfilmentByIdQuery.Fulfilment getFulfilmentById(Context context,String fulfilmentId){
        GetFulfilmentByIdQuery fulfilmentByIdQuery = GetFulfilmentByIdQuery.builder().id(fulfilmentId).build();
        GetFulfilmentByIdQuery.Data fulfilmentData = (GetFulfilmentByIdQuery.Data)context.api().query(fulfilmentByIdQuery);
        GetFulfilmentByIdQuery.Fulfilment fulfilment = fulfilmentData.fulfilment();
        return fulfilment;
    }

    /**
     *
     * @param context
     * @param fulfilmentRefs
     * @return returns fulfilment
     */
    public static GetFulfilmentByRefQuery.Fulfilments getFulfilmentsByRef(Context context,List<String> fulfilmentRefs){
        GetFulfilmentByRefQuery fulfilmentByRefQuery = GetFulfilmentByRefQuery.builder().fulfilmentRef(fulfilmentRefs).build();
        GetFulfilmentByRefQuery.Data fulfilmentsByRefData = (GetFulfilmentByRefQuery.Data)context.api().query(fulfilmentByRefQuery);
        GetFulfilmentByRefQuery.Fulfilments fulfilments = fulfilmentsByRefData.fulfilments();
        return fulfilments;
    }

    /**
     *
     * @param context
     * @param fulfilmentId
     * @param attributeName
     * @return returns true if attribute exists
     */
    public static boolean checkIfAttributeExist(Context context,String fulfilmentId,String attributeName){
        GetFulfilmentByIdQuery.Fulfilment fulfilment = getFulfilmentById(context, fulfilmentId);
        if(fulfilment != null ){
            Optional<GetFulfilmentByIdQuery.Attribute> anyFulfilmentAttribute = fulfilment.attributes().stream().filter(fulfilmentAttribute -> fulfilmentAttribute.name().equalsIgnoreCase(attributeName)).findAny();
            return  anyFulfilmentAttribute.isPresent();
        }
        return false;
    }

    /**
     *
     * @param context
     * @param fulfilmentId
     * @param attributeName
     * @return returns the attribute if found.
     */
    public static Object getAttribute(Context context,String fulfilmentId,String attributeName){
        GetFulfilmentByIdQuery.Fulfilment fulfilment = getFulfilmentById(context, fulfilmentId);

        if(fulfilment != null){
            Optional<GetFulfilmentByIdQuery.Attribute> anyFulfilmentAttribute = fulfilment.attributes().stream().filter(fulfilmentAttribute -> fulfilmentAttribute.name().equalsIgnoreCase(attributeName)).findAny();
            if(anyFulfilmentAttribute.isPresent()){
                return anyFulfilmentAttribute.get().value();
            }
        }
        return null;
    }

    public static String getCustomerRef(Context context,String fulfilmentId){
        GetFulfilmentByIdQuery.Fulfilment fulfilment = getFulfilmentById(context, fulfilmentId);
        String orderId = fulfilment.order().id();
        return OrderUtils.getCustomerRef(context,orderId);
    }


    }
