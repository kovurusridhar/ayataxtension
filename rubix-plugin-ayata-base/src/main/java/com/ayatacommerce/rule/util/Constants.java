package com.ayatacommerce.rule.util;

/**
 * Use this class to define all the constants you will use in your rules!
 */
public final class Constants {

    private Constants() { }

    public static final String ATTRIBUTE_NAME_PARAMETER_NAME = "name";

    public static final String ATTRIBUTE_VALUE_PARAMETER_NAME = "value";

    public static final String ATTRIBUTE_TYPE_PARAMETER_NAME = "type";

    public static final String EVENT_NAME_PARAMETER_NAME = "eventName";

    public static String BLANK = "";
    public static String COLON =":";

    public static String EXCEPTION_TYPE = "exception_type";
    public static String EXCEPTION_MESSAGE = "exception_message";


    public interface Rule_Config_Properties {
        String EVENT_NAME = "eventName";
        String COMMENT = "comment";
        String VIRTUAL_CATALOGUE ="VIRTUAL_CATALOGUE";
        String PROP_INVENTORY_CATALOGUE_REF = "inventoryCatalogueRef";
        String TRANSACTION_TYPE = "transactionType";
        String ATTRIBUTE_NAME ="attributeName";
        String ATTRIBUTE_VALUE ="attributeValue";


    }
    public interface Common_Constants{
        String CONDITION_SATISFIED_EVENT  = "conditionSatisfiedEvent";
        String CONDITION_UNSATISFIED_EVENT  = "conditionUnsatisfiedEvent";
        String REF ="ref";
        String LOCATION_REF ="locationRef";

    }
    public interface EntityType {
        String ENTITY_TYPE_FULFILMENT = "FULFILMENT";
        String ENTITY_TYPE_FULFILMENT_OPTIONS = "FULFILMENT_OPTIONS";
        String ENTITY_TYPE_FULFILMENT_PLAN = "FULFILMENT_PLAN";
        String ENTITY_TYPE_LOCATION = "LOCATION";
        String ENTITY_TYPE_ORDER = "ORDER";
        String ENTITY_TYPE_INVOICE ="INVOICE";
        String ENTITY_TYPE_CREDIT_MEMO = "CREDIT_MEMO";
        String ENTITY_TYPE_BILLING_ACCOUNT = "BILLING_ACCOUNT";
        String ENTITY_TYPE_STRING ="STRING";
        String ENTITY_TYPE_JSON ="JSON";
        String ENTITY_TYPE_INTEGER ="INTEGER";
    }
    public interface Fulfilment_Constants {
        String FULFILMENT_ID = "fulfilmentId";
        String FULFILMENT_REF = "fulfilmentRef";
    }
    public interface Invoice_Constants {
        String AMOUNT_TO_CAPTURE = "amountToCapture";
        String INVOICE_REF = "invoiceRef";
    }
    public interface Order_Constants{
        String ORDER_ID = "orderId";
        String ORDER_REF = "orderRef";

        public static final String FIRST_FULFILMENT_SHIPPED ="firstFulfilmentShipped";

        public interface FinancialTransaction_Constants{
            public static final String AUTHORIZED = "AUTHORIZED";
            public static final String CAPTURE = "CAPTURE";
            public static final String CAPTURED = "CAPTURED";
            public static final String FINANCIAL_TRANSACTION_REF ="financialTransactionRef";
        }

    }

}
