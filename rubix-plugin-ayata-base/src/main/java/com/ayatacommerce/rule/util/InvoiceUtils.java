package com.ayatacommerce.rule.util;

import com.apollographql.apollo.api.Operation;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.invoice.GetInvoiceByRefQuery;
import com.fluentretail.rubix.v2.context.Context;

import java.util.Optional;

public class InvoiceUtils {
    private InvoiceUtils(){

    }

    /**
     *
     * @param context
     * @param invoiceRef
     * @return returns invoice
     */
    public static GetInvoiceByRefQuery.Invoice getInvoiceByRef(Context context,String invoiceRef){
        GetInvoiceByRefQuery getInvoiceByRefQuery = GetInvoiceByRefQuery.builder().invoiceRef(invoiceRef).build();
        GetInvoiceByRefQuery.Data invoiceData = (GetInvoiceByRefQuery.Data)context.api().query(getInvoiceByRefQuery);
        if(invoiceData != null && invoiceData.invoice() != null){
            return invoiceData.invoice();
        }
        return null;
    }

    /**
     *
     * @param context
     * @param invoiceRef
     * @param attributeName
     * @return returns invoice attribute value
     */
    public static Object getAttribute(Context context,String invoiceRef,String attributeName){
        GetInvoiceByRefQuery.Invoice invoiceByRef = getInvoiceByRef(context, invoiceRef);
        if(invoiceByRef != null){
            Optional<GetInvoiceByRefQuery.Attribute> anyInvoiceAttribute = invoiceByRef.attributes().stream().filter(invoiceAttribute -> invoiceAttribute.name().equalsIgnoreCase(attributeName)).findAny();
            if(anyInvoiceAttribute.isPresent()){
                return anyInvoiceAttribute.get().value();
            }
        }

        return null;
    }

    /**
     *
     * @param context
     * @param invoiceRef
     * @param attributeName
     * @return
     */
    public static boolean checkIfAttributeExist(Context context,String invoiceRef,String attributeName){
        GetInvoiceByRefQuery.Invoice invoiceByRef = getInvoiceByRef(context, invoiceRef);
        if(invoiceByRef != null){
            Optional<GetInvoiceByRefQuery.Attribute> anyInvoiceAttribute = invoiceByRef.attributes().stream().filter(invoiceAttribute -> invoiceAttribute.name().equalsIgnoreCase(attributeName)).findAny();
            return  anyInvoiceAttribute.isPresent();
        }
        return false;
    }



    }
