package com.ayatacommerce.rule.common;


import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.ayatacommerce.rule.util.FulfilmentUtils;
import com.ayatacommerce.rule.util.OrderUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

import static com.ayatacommerce.rule.util.Constants.EntityType.*;
import static com.ayatacommerce.rule.util.Constants.Invoice_Constants.INVOICE_REF;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;

@RuleInfo(
        name = "SendEventToInvoice",
        description = "Sends an event to Invoice with the event name  {"+ EVENT_NAME+"} ",
        produces = {@EventInfo(entityType = "INVOICE", eventName = "{" + EVENT_NAME + "}")},
        accepts = {@EventInfo(entityType = ENTITY_TYPE_ORDER),
                @EventInfo(entityType = ENTITY_TYPE_FULFILMENT)
        }
)
/**
 *  Order or Fuflilment should have an attribute with name invoiceRef
 */

@ParamString(name = EVENT_NAME, description = "Event name")

@Slf4j
public class SendEventToInvoice implements Rule {

    public static String CLASS_NAME =SendEventToInvoice.class.getSimpleName();

    @Override
    public void run(Context context) {
        AyataRuleUtils.validateRulePropsIsNotEmpty(context,EVENT_NAME);
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        String entityType = contextEvent.getEntityType();
        String entityId = contextEvent.getEntityId();
        String invoiceRef = null;
        String customerRef = null;
        switch (entityType){
            case ENTITY_TYPE_ORDER:
                Object orderInvoiceRefAttribute = OrderUtils.getAttribute(context, entityId, INVOICE_REF);
                if(orderInvoiceRefAttribute != null){
                    invoiceRef = orderInvoiceRefAttribute.toString();
                    customerRef = OrderUtils.getCustomerRef(context,entityId);
                }
                break;
            case ENTITY_TYPE_FULFILMENT:
                Object fulfilmentInvoiceRefAttribute = FulfilmentUtils.getAttribute(context, entityId, INVOICE_REF);
                if(fulfilmentInvoiceRefAttribute != null){
                    invoiceRef = fulfilmentInvoiceRefAttribute.toString();
                    customerRef = FulfilmentUtils.getCustomerRef(context,entityId);
                }
            default:
            {
                log.debug(logPrefix +"Illegal type configured");
                throw  new IllegalArgumentException(logPrefix +" Illegal Type configured");
            }
        }

        if(StringUtils.isNotEmpty(invoiceRef)){
            Event event = Event.builder()
                    .id(UUID.randomUUID())
                    .entityRef(invoiceRef)
                    .entityType(ENTITY_TYPE_INVOICE)
                    .accountId(contextEvent.getAccountId())
                    .retailerId(contextEvent.getRetailerId())
                    .rootEntityRef(customerRef)
                    .rootEntityType(ENTITY_TYPE_BILLING_ACCOUNT)
                    .attributes(contextEvent.getAttributes()).build();
            log.debug(logPrefix +" Sending Event to Invoice ",event);
        }

    }
}
