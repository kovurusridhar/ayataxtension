package com.ayatacommerce.rule.common;

import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.fulfilment.GetFulfilmentByIdQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import static com.ayatacommerce.rule.util.Constants.EntityType.ENTITY_TYPE_FULFILMENT;
import static com.ayatacommerce.rule.util.Constants.EntityType.ENTITY_TYPE_ORDER;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.ATTRIBUTE_NAME;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;
import static com.ayatacommerce.rule.util.OrderUtils.getOrderByIdQuery;


@RuleInfo(
        name = "SendEventWithAttributes",
        description = "ReSends Event {"+ EVENT_NAME+"} with  attributes ",
        produces = {
                @EventInfo(entityType = "BILLING_ACCOUNT",
                        eventName = "{" + EVENT_NAME + "}",
                        entitySubtype = "BILLING_ACCOUNT",
                        status = "CREATED")
        },
        accepts = {@EventInfo(entityType = ENTITY_TYPE_ORDER),
                   @EventInfo(entityType = ENTITY_TYPE_FULFILMENT)
                  }
)

@ParamString(name = EVENT_NAME, description = "Event name")

@Slf4j
public class SendEventWithAttributes  implements Rule {

    public static String CLASS_NAME = SendEventWithAttributes.class.getSimpleName();

    @Override
    public void run(Context context){
        AyataRuleUtils.validateRulePropsIsNotEmpty(context,EVENT_NAME);

        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        String entityType = contextEvent.getEntityType();
        String entityStatus = null;
        if(entityType.equalsIgnoreCase(ENTITY_TYPE_ORDER)){
            final GetOrderByIdQuery.Data orderData = getOrderByIdQuery(context,
                    contextEvent.getEntityId(),
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false
            );
            GetOrderByIdQuery.Order order = orderData.order();
            entityStatus = order.status();
        }else if(entityType.equalsIgnoreCase(ENTITY_TYPE_FULFILMENT)){

            GetFulfilmentByIdQuery fulfilmentByIdQuery = GetFulfilmentByIdQuery.builder().id(contextEvent.getEntityId()).build();
            GetFulfilmentByIdQuery.Data fulfilmentData = (GetFulfilmentByIdQuery.Data)context.api().query(fulfilmentByIdQuery);
            entityStatus = fulfilmentData.fulfilment().status();
        }

        Event event = contextEvent.toBuilder().name(context.getProp(EVENT_NAME)).attributes(contextEvent.getAttributes()).entityStatus(entityStatus).build();

        log.info(logPrefix +" Sending event "+ event);
        context.action().sendEvent(event);
    }
}