package com.ayatacommerce.rule;

import com.ayatacommerce.rule.util.Constants;
import com.fluentretail.api.model.RubixEntity;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.test.TestContext;
import com.fluentretail.rubix.v2.test.TestExecutor;
import com.fluentretail.rubix.workflow.RuleSet;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.UUID;

import static com.fluentretail.rubix.test.TestUtils.*;
import static junit.framework.TestCase.assertSame;

public class DemoAddAttributeToOrderTest {

    @Test
    public void addAttributeToOrder() {
        // Given
        RubixEntity entity = RubixEntity.builder()
                .status("INACTIVE")
                .entityType("ORDER")
                .flexType("flexType")
                .flexVersion(1)
                .ref("1")
                .id(UUID.randomUUID().toString())
                .build();

        Event event = Event.builder()
                .retailerId("1")
                .entityId(entity.getId())
                .rootEntityRef(entity.getRef())
                .entityType(entity.getEntityType())
                .build();

        ImmutableMap.Builder<String, Object> propertyBuilder = ImmutableMap.builder();
        propertyBuilder.put(Constants.ATTRIBUTE_NAME_PARAMETER_NAME, "prop-name");
        propertyBuilder.put(Constants.ATTRIBUTE_TYPE_PARAMETER_NAME, "prop-type");
        propertyBuilder.put(Constants.ATTRIBUTE_VALUE_PARAMETER_NAME, "prop-value");

        RuleSet ruleSet = ruleSet(
                event,
                ruleInstance(
                        DemoAddAttributeToOrder.class.getAnnotation(RuleInfo.class).name(),
                        propertyBuilder.build()
                )
        );

        // Set up a TestExecutor to simulate Orchestration Engine
        TestExecutor executor = TestExecutor.builder()
                .rule(DemoAddAttributeToOrder.class)
                .ruleset(ruleSet)
                .entity(entity)
                .build();

        executor.validateWorkflow(event);

        // When
        TestContext execute = executor.execute(event);

        // Then
        assertSame(execute.getEvent(), event);
    }
}