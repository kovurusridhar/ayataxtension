package com.ayatacommerce.rule.util;

import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.mutations.comment.CreateCommentMutation;
import com.fluentretail.rubix.foundation.graphql.type.CreateCommentInput;
import com.fluentretail.rubix.v2.context.Context;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class CommentUtils {

    private CommentUtils(){

    }

    public static void addCommentToEntity(Context context, String comment) {
        addCommentToEntity(context, comment, context.getEntity().getEntityType(), context.getEntity().getId());
    }

    public static void addCommentToEntity(Context context, String comment, String entityType, String entityId) {
        if (StringUtils.isNotBlank(comment)) {
            CreateCommentInput input = CreateCommentInput.builder().entityId(entityId).entityType(entityType).text(comment).build();
            CreateCommentMutation mutation = CreateCommentMutation.builder().input(input).build();
            context.action().mutation(mutation);
        }
    }
}