package com.ayatacommerce.rule.util;

import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.setting.QuerySettingsQuery;
import com.fluentretail.rubix.v2.context.Context;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;


import java.util.List;
import java.util.Optional;

import static java.lang.Integer.valueOf;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Slf4j
public class SettingsUtils {

    private SettingsUtils(){

    }

    public static String getSettingValue(Context context, String settingName, String defaultValue) {
        if (isBlank(settingName)) {
            log.info("Setting name is empty, using default value {}", defaultValue);
            return defaultValue;
        }
        try {
            QuerySettingsQuery.Edge settingEdge = getSetting(context, settingName);
            if (settingEdge != null) {
                return settingEdge.node().value();
            }
        } catch (Exception e) {
            log.error("Exception occured while executing " + SettingsUtils.class.getSimpleName(), e);
        }
        return defaultValue;
    }

    public static Integer getSettingValue(Context context, String settingName, Integer defaultValue) {
        if (isBlank(settingName)) {
            log.info("Setting name is empty, using default value {}", defaultValue);
            return defaultValue;
        }
        try {
            QuerySettingsQuery.Edge settingEdge = getSetting(context, settingName);
            if (settingEdge != null) {
                return valueOf(settingEdge.node().value());
            }
        } catch (Exception e) {
            log.error("Exception occured while executing, use default value " + SettingsUtils.class.getSimpleName(), e);
        }
        return defaultValue;
    }

    public static Object getSettingLobValue(Context context, String settingName){
        log.info("[COMMON-GI]: {} Getting setting LobValue for Key:{}"
                , SettingsUtils.class.getSimpleName()
                , settingName);
        QuerySettingsQuery.Edge settingsEdge = getSetting(context, settingName);
        if(settingsEdge != null){
            return settingsEdge.node().value();
        }
        return null;
    }

    private static QuerySettingsQuery.Edge getSetting(Context context, String settingName) {
        List<QuerySettingsQuery.Edge> edges = getSettingData(context, "RETAILER", context.getEvent().getRetailerId(), settingName).settings().edges();
        log.info("[COMMON-GI]: {} RETAILER setting:{}", SettingsUtils.class.getSimpleName(), edges);
        if (CollectionUtils.isEmpty(edges)) {
            edges = getSettingData(context, "ACCOUNT", "0", settingName).settings().edges();
            log.info("[COMMON-GI]: {} ACCOUNT setting:{}", SettingsUtils.class.getSimpleName(), edges);
            if (CollectionUtils.isEmpty(edges)) {
                edges = getSettingData(context, "GLOBAL", "0", settingName).settings().edges();
                log.info("[COMMON-GI]: {} GLOBAL setting:{}", SettingsUtils.class.getSimpleName(), edges);
            }
        }
        Optional<QuerySettingsQuery.Edge> optEdge = edges.stream().findFirst();
        if (optEdge.isPresent())
            return optEdge.get();

        return null;
    }

    private static QuerySettingsQuery.Data getSettingData(Context context, String settingContext, String contextId,  String settingName) {

        QuerySettingsQuery querySettings = QuerySettingsQuery.builder().name(ImmutableList.of(settingName)).context(ImmutableList.of(settingContext))
                .contextId(ImmutableList.of(Integer.parseInt(contextId))).build();
        log.info("[COMMON-GI]: {} - GrapQL Params - settingNameList: {}, contextList: {} contextIdList :{}"
                , SettingsUtils.class.getSimpleName()
                , settingName
                , settingContext
                , Long.parseLong(contextId));

        return (QuerySettingsQuery.Data) context.api().query(querySettings);
    }

}
