package com.ayatacommerce.rule.util;

import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.virtualPositions.GetVpsOfProductQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.virtualPositions.GetVpsOfProductWithStatusQuery;
import com.fluentretail.rubix.foundation.graphql.type.VirtualCatalogueKey;
import com.fluentretail.rubix.v2.context.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VirtualCatalogueUtils {

    private VirtualCatalogueUtils(){

    }

    /**
     *
     * @param productRef ref of the product
     * @param virtualCatalogueRef ref of the virtual catalogue
     * @param context context of the rule
     * @return returns all virtual positions of the product
     */
    public static List<GetVpsOfProductQuery.Node> getVpsOfProduct(String productRef, String virtualCatalogueRef, Context context){
        List<String> productRefs = new ArrayList<>();
        productRefs.add(productRef);

        GetVpsOfProductQuery vpsOfProductQuery = GetVpsOfProductQuery.builder().productRefs(productRefs).catalogueRef(VirtualCatalogueKey.builder().ref(virtualCatalogueRef).build()).build();
        GetVpsOfProductQuery.Data vpsOfProductData = (GetVpsOfProductQuery.Data)context.api().query(vpsOfProductQuery);
        List<GetVpsOfProductQuery.Node> vpNodes = vpsOfProductData.virtualPositions().edges().stream().map(virtualPositionEdge -> virtualPositionEdge.node()).collect(Collectors.toList());
        return vpNodes;
    }

    /**
     *
     * @param productRefs all product refs
     * @param virtualCatalogueRef ref of the virtual catalogue
     * @param context context of the rule
     * @return returns all virtual positions of all the products
     */
    public static List<GetVpsOfProductQuery.Node> getVpsOfProducts(List<String> productRefs, String virtualCatalogueRef, Context context){
        GetVpsOfProductQuery vpsOfProductQuery = GetVpsOfProductQuery.builder().productRefs(productRefs).catalogueRef(VirtualCatalogueKey.builder().ref(virtualCatalogueRef).build()).build();
        GetVpsOfProductQuery.Data vpsOfProductData = (GetVpsOfProductQuery.Data)context.api().query(vpsOfProductQuery);
        List<GetVpsOfProductQuery.Node> vpNodes = vpsOfProductData.virtualPositions().edges().stream().map(virtualPositionEdge -> virtualPositionEdge.node()).collect(Collectors.toList());
        return vpNodes;
    }

    /**
     *
     * @param productRefs all product refs
     * @param virtualCatalogueRef ref of the virtual catalouge
     * @param statuses all statuses
     * @param context context of the rule
     * @return returns all the virtual postions of all products with the mentioned statuses
     */
    public static List<GetVpsOfProductWithStatusQuery.Node> getVpsOfProductWithStatus(List<String> productRefs, String virtualCatalogueRef, List<String> statuses , Context context){

        GetVpsOfProductWithStatusQuery vpsOfStatusesQuery = GetVpsOfProductWithStatusQuery.builder().productRefs(productRefs).virtualCatalogueRef(VirtualCatalogueKey.builder().ref(virtualCatalogueRef).build()).status(statuses).build();
        GetVpsOfProductWithStatusQuery.Data vpsOfProductData = (GetVpsOfProductWithStatusQuery.Data)context.api().query(vpsOfStatusesQuery);
        List<GetVpsOfProductWithStatusQuery.Node> vpsWithStatuses = vpsOfProductData.virtualPositions().edges().stream().map(virtualPositionEdge -> virtualPositionEdge.node()).collect(Collectors.toList());
        return vpsWithStatuses;
    }
}
