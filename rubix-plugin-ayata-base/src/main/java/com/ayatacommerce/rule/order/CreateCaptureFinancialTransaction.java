package com.ayatacommerce.rule.order;

import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.mutations.CreateFinancialTransactionMutation;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.foundation.graphql.type.CreateFinancialTransactionInput;
import com.fluentretail.rubix.foundation.graphql.type.OrderId;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Optional;

import static com.ayatacommerce.rule.util.Constants.Invoice_Constants.AMOUNT_TO_CAPTURE;
import static com.ayatacommerce.rule.util.Constants.Order_Constants.FinancialTransaction_Constants.*;
import static com.ayatacommerce.rule.util.OrderUtils.getAnyAuthorizedTransactionByOrderId;

@RuleInfo(
        name = "CreateCaptureFinancialTransaction",
        description = "Create Capture Financial Transaction Entry",
        accepts = {@EventInfo(entityType = "ORDER")}

)
@Slf4j
public class CreateCaptureFinancialTransaction implements Rule {
    public static String CLASS_NAME = CreateCaptureFinancialTransaction.class.getSimpleName();

    public void run(Context context) {
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        log.debug(logPrefix + " Creating Capture Financial Transaction");
        Map<String, Object> eventAttributes = contextEvent.getAttributes();
        if (MapUtils.isNotEmpty(eventAttributes)) {
            Double amountToCapture = getAmountToCapture(contextEvent);

            if (amountToCapture != null && amountToCapture > 0) {
                Optional<GetOrderByIdQuery.Edge> anyAuthorizedTransaction = getAnyAuthorizedTransactionByOrderId(context);
                if (anyAuthorizedTransaction.isPresent()) {
                    GetOrderByIdQuery.Node authorizedPayment = anyAuthorizedTransaction.get().node();
                    final String transactionRef = getUniqueTransactionRef(contextEvent);

                    CreateFinancialTransactionInput transactionInput = CreateFinancialTransactionInput.builder()
                            .ref(transactionRef)
                            .type(authorizedPayment.type())
                            .paymentMethod(authorizedPayment.paymentMethod())
                            .amount(amountToCapture)
                            .cardType(authorizedPayment.cardType())
                            .currency(authorizedPayment.currency())
                            .externalTransactionCode(authorizedPayment.externalTransactionCode())
                            .externalTransactionId(authorizedPayment.externalTransactionId())
                            .order(OrderId.builder().id(context.getEvent().getEntityId()).build())
                            .build();

                    CreateFinancialTransactionMutation mutation = CreateFinancialTransactionMutation.builder()
                            .input(transactionInput)
                            .build();
                    log.debug(logPrefix + " Creating Capture financial Transaction {} " , transactionRef);
                    context.action().mutation(mutation);
                }
            }
        }

    }


    private Double getAmountToCapture(Event contextEvent) {
        String amountToCapture = contextEvent.getAttribute(AMOUNT_TO_CAPTURE);
        if (StringUtils.isNotEmpty(amountToCapture)) {
            return Double.parseDouble(amountToCapture);
        }
        return null;
    }

    private String getUniqueTransactionRef(Event contextEvent) {
        String financialTransactionRef = contextEvent.getAttribute(FINANCIAL_TRANSACTION_REF);
        if (StringUtils.isNotEmpty(financialTransactionRef)) {
            return financialTransactionRef;
        }
        return null;
    }


}

