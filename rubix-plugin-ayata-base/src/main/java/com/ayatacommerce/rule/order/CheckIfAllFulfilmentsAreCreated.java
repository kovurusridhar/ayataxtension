package com.ayatacommerce.rule.order;

import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.ayatacommerce.rule.util.CommonUtils;
import com.ayatacommerce.rule.util.OrderUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

import static com.ayatacommerce.rule.util.Constants.Common_Constants.*;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;


@RuleInfo(
        name = "CheckIfAllFulfilmentsAreCreated",
        description = "Checks if for all order items fulfilments are associated  if yes then sends an event {" + CONDITION_SATISFIED_EVENT + "} otherwise sends  {" + CONDITION_UNSATISFIED_EVENT + "}",
        accepts = {@EventInfo(entityType = "ORDER")})


@ParamString(name = CONDITION_SATISFIED_EVENT, description = "Sends an event when condition is  satisfied  other same event rescheduled ")
@ParamString(name = CONDITION_UNSATISFIED_EVENT, description = "Trigger same event if not all fulfilment are created")

@Slf4j
public class CheckIfAllFulfilmentsAreCreated implements Rule {

    public static final String CLASS_NAME = CheckIfAllFulfilmentsAreCreated.class.getSimpleName();

    @Override
    public void run(Context context) {
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, CONDITION_SATISFIED_EVENT,CONDITION_UNSATISFIED_EVENT);

        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        String orderId = contextEvent.getEntityId();

        GetOrderByIdQuery.Data orderData = OrderUtils.getOrderByIdQuery(context, orderId,
                true,
                false,
                false,
                true,
                true,
                false,
                false,
                false);
        GetOrderByIdQuery.Order order = orderData.order();
        // storing all items product ref and quantity ordered
        Map<String, Integer> itemsMap = new HashMap<>();
        order.items().itemEdges().stream().forEach(itemEdge -> {
            itemsMap.put(itemEdge.itemNode().product().asVariantProduct().ref(), itemEdge.itemNode().quantity());
        });

        order.fulfilments().fulfilmentEdges().stream().forEach(fulfilmentEdge -> {
            GetOrderByIdQuery.FulfilmentItems fulfilmentItems = fulfilmentEdge.fulfilmentNode().fulfilmentItems();
            fulfilmentItems.fulfilmentItemEdges().stream().forEach(fulfilmentItemEdge -> {
                String productRef = fulfilmentItemEdge.fulfilmentItemNode().orderItem().product().asVariantProduct().ref();
                Integer fulfilmentRequestedQty = fulfilmentItemEdge.fulfilmentItemNode().requestedQuantity();
                Integer orderedQty = itemsMap.get(productRef);
                if(orderedQty != null){
                    if (orderedQty == fulfilmentRequestedQty) {
                        itemsMap.remove(productRef);
                    } else {
                        int fulfilmentItemQty = orderedQty - fulfilmentRequestedQty;
                        itemsMap.put(productRef, fulfilmentItemQty);
                    }
                }

            });
        });
        Event event= null;

        //  if all items fulfilment  are created then map should be empty
        if (itemsMap.isEmpty()) {
            // for all items fulfilments  are created
            event = contextEvent.toBuilder().name(context.getProp(CONDITION_SATISFIED_EVENT)).build();
        } else {
            event = contextEvent.toBuilder().name(context.getProp(CONDITION_UNSATISFIED_EVENT)).build();
        }

        if (event != null) {
            log.info(logPrefix + "Sending Event" + event);
            context.action().sendEvent(event);
        }


    }

}
