package com.ayatacommerce.rule.invoice;

import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.invoice.GetInvoiceByRefQuery;
import com.fluentretail.rubix.foundation.graphql.com.ayatacommerce.queries.order.GetOrderByIdQuery;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

import static com.ayatacommerce.rule.util.Constants.EntityType.ENTITY_TYPE_ORDER;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;
import static com.ayatacommerce.rule.util.OrderUtils.getOrderByRef;

@RuleInfo(
        name = "SendEventToOrderFromInvoice",
        description = "Sends an event to the Fulfilment from invoice entity with the event name{" + EVENT_NAME + "}",
        accepts = {@EventInfo(entityType = "INVOICE")},
        produces = {@EventInfo(entityType = "ORDER", eventName = "{" + EVENT_NAME + "}")}
)

@Slf4j
public class SendEventToOrderFromInvoice implements Rule {

    public static String CLASS_NAME = SendEventToOrderFromInvoice.class.getSimpleName();
    @Override
    public void run(Context context) {
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        log.debug(logPrefix,contextEvent);
        GetInvoiceByRefQuery invoiceByRefQuery = GetInvoiceByRefQuery.builder().invoiceRef(contextEvent.getEntityRef()).build();
        GetInvoiceByRefQuery.Data invoiceRefData = (GetInvoiceByRefQuery.Data) context.api().query(invoiceByRefQuery);

        if (invoiceRefData != null && invoiceRefData.invoice() != null) {
            GetInvoiceByRefQuery.Invoice invoice = invoiceRefData.invoice();
                GetOrderByIdQuery.Order order = getOrderByRef(context, invoice.order().ref());
                Event eventToOrder = Event.builder()
                        .name(context.getProp(EVENT_NAME))
                        .accountId(contextEvent.getAccountId())
                        .retailerId(contextEvent.getRetailerId())
                        .entityId(order.id())
                        .entityRef(order.ref())
                        .entityType(ENTITY_TYPE_ORDER)
                        .scheduledOn(new Date())
                        .attributes(contextEvent.getAttributes())
                        .build();
                log.debug(logPrefix +" Sending event to fulfilment ",eventToOrder);
                context.action().sendEvent(eventToOrder);

        }
    }
}
