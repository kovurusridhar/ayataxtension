package com.ayatacommerce.rule.common;


import com.ayatacommerce.rule.util.AyataRuleUtils;
import com.ayatacommerce.rule.util.FulfilmentUtils;
import com.ayatacommerce.rule.util.InvoiceUtils;
import com.ayatacommerce.rule.util.OrderUtils;
import com.fluentretail.rubix.event.Event;
import com.fluentretail.rubix.rule.meta.EventInfo;
import com.fluentretail.rubix.rule.meta.ParamString;
import com.fluentretail.rubix.rule.meta.RuleInfo;
import com.fluentretail.rubix.v2.context.Context;
import com.fluentretail.rubix.v2.rule.Rule;
import lombok.extern.slf4j.Slf4j;

import static com.ayatacommerce.rule.util.Constants.EntityType.*;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.ATTRIBUTE_NAME;
import static com.ayatacommerce.rule.util.Constants.Rule_Config_Properties.EVENT_NAME;

@RuleInfo(
        name = "SendEventIfAttributeExist",
        description = "Sends an event if attribute with name {" + ATTRIBUTE_NAME + "} then send event {" + EVENT_NAME + "}",
        accepts = {@EventInfo(entityType = "ORDER"),
                @EventInfo(entityType = "FULFILMENT"),
                @EventInfo(entityType = "INVOICE")
        }
)

@ParamString(name = ATTRIBUTE_NAME, description = "Name of the attribute to check")
@ParamString(name = EVENT_NAME, description = "Event to generate when attribute found")

@Slf4j
public class SendEventIfAttributeExist implements Rule {

    public static String CLASS_NAME = SendEventIfAttributeExist.class.getSimpleName();

    @Override
    public void run(Context context) {
        AyataRuleUtils.validateRulePropsIsNotEmpty(context, ATTRIBUTE_NAME, EVENT_NAME);
        Event contextEvent = context.getEvent();
        String logPrefix = AyataRuleUtils.buildLogPrefix(CLASS_NAME, contextEvent);
        log.debug(logPrefix);
        String currentEntityType = contextEvent.getEntityType();
        String configuredAttribute = context.getProp(ATTRIBUTE_NAME);
        String eventToGenerate = context.getProp(EVENT_NAME);
        String entityId = contextEvent.getEntityId();
        Event event = null;

        switch (currentEntityType) {
            case ENTITY_TYPE_ORDER:
                boolean orderAttributeExists = OrderUtils.checkIfAttributeExist(context,entityId,configuredAttribute);
                if(orderAttributeExists){
                    event = contextEvent.toBuilder().name(eventToGenerate).build();
                }
                break;
            case ENTITY_TYPE_FULFILMENT:
                boolean fulfilmentAttributeExists = FulfilmentUtils.checkIfAttributeExist(context, entityId, configuredAttribute);
                if(fulfilmentAttributeExists){
                    event = contextEvent.toBuilder().name(eventToGenerate).build();
                }
                break;
            case ENTITY_TYPE_INVOICE:
                String invoiceRef = contextEvent.getEntityRef();
                boolean invoiceAttributeExists = InvoiceUtils.checkIfAttributeExist(context, invoiceRef, configuredAttribute);
                if(invoiceAttributeExists){
                    event = contextEvent.toBuilder().name(eventToGenerate).build();
                }
                break;
            default:
                throw new IllegalArgumentException(logPrefix + "Invalid type configured ");
        }

        if(event != null){
            log.debug(logPrefix +" Sending Event ",event);
            context.action().sendEvent(event);
        }


    }
}
